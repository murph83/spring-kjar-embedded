package org.jbpm.spring.boot;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ComponentScan
@EnableAutoConfiguration(exclude=HibernateJpaAutoConfiguration.class)
@ImportResource(value= {"classpath:config/jee-tx-context.xml", "classpath:config/jbpm-context.xml",})
public class Application extends SpringBootServletInitializer {
	
    @Override
    protected SpringApplicationBuilder configure(
    		SpringApplicationBuilder builder) {
    	builder.application().addListeners(new DeploymentListener());
    	return builder.sources(Application.class);
    }
}
