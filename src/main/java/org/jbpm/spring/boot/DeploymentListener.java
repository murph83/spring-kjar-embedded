package org.jbpm.spring.boot;

import org.jbpm.kie.services.impl.KModuleDeploymentUnit;
import org.jbpm.services.api.DeploymentService;
import org.jbpm.services.api.model.DeployedUnit;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class DeploymentListener implements
		ApplicationListener<ApplicationReadyEvent> {

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		DeploymentService deploymentService = event.getApplicationContext()
				.getBean(DeploymentService.class);
		try {
			deploymentService.deploy(new KModuleDeploymentUnit("example","test", "1.0"));
		} catch (RuntimeException re) {
			
		}
	}
}
