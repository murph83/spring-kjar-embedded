package org.jbpm.spring.boot;

import org.jbpm.services.api.ProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/process")
public class ProcessController {
	
	@Autowired
	ProcessService processService;
	
	@RequestMapping("/start")
	@ResponseBody
	public Long start(){
		return processService.startProcess("example:test:1.0", "test.process");
		
	}

}
