package org.jbpm;

import java.util.Collections;
import java.util.List;

import org.kie.internal.identity.IdentityProvider;

public class SystemIdentityProvider implements IdentityProvider{

	@Override
	public String getName() {
		return "system";
	}

	@Override
	public List<String> getRoles() {
		return Collections.emptyList();
	}

	@Override
	public boolean hasRole(String role) {
		return false;
	}
	

}
